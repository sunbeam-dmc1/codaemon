package com.dmc.bitcoin;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import java.util.List;

public class BitcoinAdapter extends RecyclerView.Adapter<BitcoinAdapter.ViewHolder> {
    List<bitcoindata> countryList;

    public BitcoinAdapter(List<bitcoindata> countryList) {
        this.countryList = countryList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.country_list, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        bitcoindata country = countryList.get(position);
        holder.textcode.setText(country.getCode());
        holder.textcountry_symbol.setText("Country_symbol : "+country.getCurrency_symbol());
        holder.textrate.setText("textrate : "+country.getRate());
        holder.textdiscription.setText("Discription : "+country.getDiscription());

    }

    @Override
    public int getItemCount() {
        return countryList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textcode, textcountry_symbol, textrate, textdiscription;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textcode = itemView.findViewById(R.id.textcode);
            textcountry_symbol = itemView.findViewById(R.id.textcountry_symbol);
            textrate = itemView.findViewById(R.id.textrate);
            textdiscription = itemView.findViewById(R.id.textdiscription);


            itemView.setOnClickListener(this);
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            intent.putExtra("country",countryList.get(getAdapterPosition()));
            v.getContext().startActivity(intent);
        }
    }
}
